enum BlocType {
    //% block=Terre
    Terre = 0,
    //% block=Pierre
    Pierre = 1,
    //% block=Herbe
    Herbe = 2,
    //% block=Gravier
    Gravier = 3,
    //% block=Sable
    Sable = 4
}