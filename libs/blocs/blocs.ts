//% weight=70 icon="\uf1b3" color=#40F502
namespace blocs {
    /**
     * Pick a bloc
     */
    //%  blockId=blocPick block="Lorsque l'on prend un bloc de type %bloc" shim=pxsim.blocs::sendEvent
    export function onPick(bloc: BlocType) {
        sendFunctions.ipcCall(bloc.toString(), "test");
    }

    /**
     * Destroy a bloc
     */
    //% blockId=blocDestroy block="Lorsque l'on détruit un bloc de type %bloc" shim=blocs::sendEvent
    export function onDestroy(bloc: BlocType) {
        // ipcRenderer.send('sendEvent', {fonctionType: "event", fonctionName: "onDestroy",
        // blocType: bloc, });
    }
        
}