/// <reference path="../libs/core/enums.d.ts"/>
// const ipc = require('node-ipc');

namespace pxsim.sendFunctions {
    /**
     * Executes IPC to communicate with the client
     * @param component 
     * @param componentArgs 
     */
    //% promise
    export function ipcCall(instructionType: string, args: string){
        // ipc.config.id = 'hello';
        // ipc.config.retry = 1000;
        
        // ipc.connectTo(
        //     'world',
        //     function(){
        //         ipc.of.world.on(
        //             'connect',
        //             function(){
        //                 ipc.log('## connected to world ##', ipc.config.delay);
        //                 ipc.of.world.emit(
        //                     'app.message',
        //                     {
        //                         id      : ipc.config.id,
        //                         message : 'hello'
        //                     }
        //                 );
        //             }
        //         );
        //         ipc.of.world.on(
        //             'disconnect',
        //             function(){
        //                 ipc.log('disconnected from world');
        //             }
        //         );
        //         ipc.of.world.on(
        //             'app.message',
        //             function(data:any){
        //                 ipc.log('got a message from world : ', data);
        //             }
        //         );
        
        //         console.log(ipc.of.world.destroy);
        //     }
        // );
        console.log(instructionType);
        console.log("rpc send");
        return;
    }
}
